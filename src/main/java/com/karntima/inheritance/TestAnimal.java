/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.karntima.inheritance;

/**
 *
 * @author User
 */
public class TestAnimal {
    public static void main(String[] args) {
        Animal animal = new Animal("Ani","White",0);
        animal.speak();
        animal.walk();
        
        Dog dang = new Dog("Dang","Black&White");
        dang.speak();
        dang.walk();
        
        Cat zero = new Cat("Zero","Orange");
        zero.speak();
        zero.walk();
        
        Duck som = new Duck("Som","Orange");
        som.speak();
        som.walk();
        som.fly();
        
        Dog to = new Dog("To","Brown");
        to.speak();
        to.walk();
        
        Dog mome = new Dog("Mome","White&Black");
        mome.speak();
        mome.walk();
        
        Dog bat = new Dog("Bat","White&Black");
        bat.speak();
        bat.walk();
        
        Duck gabgab = new Duck("GabGab","Black&yellow");
        gabgab.speak();
        gabgab.walk();
        gabgab.fly();
        
        System.out.println("");
        System.out.println("Som is Animal: " + (som instanceof Animal));
        System.out.println("Som is Duck: " + (som instanceof Duck)) ;
        System.out.println("Som is Cat: " + (som instanceof Object));
        System.out.println("Animal is Dog: " + (animal instanceof Dog));
        System.out.println("Animal is Animal: " + (animal instanceof Animal));
        System.out.println("GabGab is Animal: " + (gabgab instanceof Animal));
        System.out.println("GabGab is Duck: " + (gabgab instanceof Duck)) ;
        System.out.println("To is Cat: " + (to instanceof Object));
        System.out.println("Bat is Animal: " + (bat instanceof Animal));
        System.out.println("Bat is bat: " + (bat instanceof Animal)) ;
        System.out.println("Mome is Cat: " + (mome instanceof Object));
    
        Animal ani1 = null;
        Animal ani2 = null;
        ani1 = som;
        ani2 = zero;
        System.out.println("Ani1: Som is Duck " + (ani1 instanceof Duck));
        
        System.out.println("***********");
        Animal[] animals = {dang,zero,som,to,mome,bat,gabgab};
        for(int i=0;i<animals.length;i++){
            animals[i].walk();
            animals[i].speak();
            if(animals[i] instanceof Duck ){
                Duck duck = (Duck)animals[i];
                duck.fly();
            }
            System.out.println("---------");
        }
        
    }
}
